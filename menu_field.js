(function ($) {

Drupal.behaviors.menu_field = {
  attach: function (context) {
    $('.menu-field-toggle-edit-form', context).once('menu-field-toggle-edit-form', function() {
      $(this).click(function(e) {
        $(this)
          .closest('tr')
          .find('.menu-field-item-edit-form')
          .slideToggle();
        e.preventDefault();
      });
    });
  }
};

})(jQuery);
