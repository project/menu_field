This module provides a field to mimic the functionality of Drupal's menu module,
from a multiple value field.  The main issue that we are trying to solve with
this implementation is that core menus are not revisionable in any way.  Updates
to menus and menu links are immediately visible, which is not desired in many
circumstances.

Field Schema
============

- id - An immutable uuid to be used as the parent value for hierarchy.
- text - The text value of the link.
- path
- parent
- enabled (bool)
- expanded (bool)
- options (serialized)

Milestones
==========

Beta 1
------

- Field schema and basic hook\_field\_* implementations.
- Basic widget using default multiple value handling.
- Basic formatter to output a standard menu tree.

Beta 2
------

- Custom multiple value handling widget, implementing hierarchical tabledrag.js
  implementation.  Additional items should be added via a form in the last row
  of the table.

Backlog
-------

- Widget support for class attribute
- Widget support for id attribute
- Widget support for _target attribute
- Support for HTML values in the text field.  This would require a field setting
  to change the text schema to use text instead of varchar, widget support to
  display a text_format widget, and formatter support to handle the sanitization
  of the value and setting 'html' => true on the link render array.
- Lightweight menu entity module to replace menu.module.
- Admin interfaces for menu entity module to replicate menu.module.
- Drop-down formatter.
